﻿using System.Collections.Generic;
using UnityEngine;

public class RoomObjects : MonoBehaviour
{
    public void Set(GameObject patternPrefab)
    {
        if (patternPrefab == null)
            return;

        var obj = Instantiate(patternPrefab);

        obj.transform.SetParent(transform);
    }

    private void Start()
    {
        if (transform.childCount <= 0)
            return;

        var objectsTransform = transform.GetChild(0);
        var toParent = new List<Transform>();
        var toDestroy = new List<GameObject>();
        foreach (Transform child in objectsTransform)
        {
            var obj = GameController.Instance.InstantiateObject(child.tag);

            if (obj != null)
            {
                obj.transform.position = child.transform.position;
                toParent.Add(obj.transform);
            }

            toDestroy.Add(child.gameObject);
        }

        foreach (var obj in toDestroy)
            Destroy(obj);

        foreach (var obj in toParent)
            obj.SetParent(objectsTransform);
    }
}
