﻿using UnityEngine;

public delegate void OnIntegerChanged(int value);
public delegate void OnVector2Changed(Vector2 value);