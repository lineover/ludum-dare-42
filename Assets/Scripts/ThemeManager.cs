﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ThemeColorName { Primary, Secondary, PrimaryLight, PrimaryDark, Accent }

public class ThemeManager : MonoBehaviour
{
    public ColorTheme CurrentTheme { get { return themes[current]; } }

    public OnVector2Changed OnLightDirectionChanged;

    [SerializeField]
    ColorTheme[] themes = {
        new ColorTheme() {
            Primary = Color.white,
            Secondary = Color.gray,
            PrimaryLight = Color.green,
            PrimaryDark = Color.gray,
            Accent = Color.red
        },
        new ColorTheme() {
            Primary = Color.white,
            Secondary = Color.gray,
            PrimaryLight = Color.green,
            PrimaryDark = Color.gray,
            Accent = Color.red
        }
    };

    int current = 0;

    Vector2 lightDirection = new Vector2(1, -2);

    public Vector2 LightDirection { get { return lightDirection; } set { lightDirection = value; if (OnLightDirectionChanged != null) OnLightDirectionChanged(lightDirection); } }

    private void Start()
    {
        GameController.Instance.OnLevelChanged += PickRandomTheme;
    }

    void PickRandomTheme(int level)
    {
        var index = current;

        while (index == current)
            index = UnityEngine.Random.Range(0, themes.Length);

        current = index;
        
        //StartCoroutine(MovingLight(2));
    }

    IEnumerator MovingLight(float duration)
    {
        var timer = 0.0f;

        var startingDir = lightDirection;
        var nextDir = new Vector2(UnityEngine.Random.Range(-1, 2), UnityEngine.Random.Range(-1, 2));

        Debug.Log(startingDir);

        Debug.Log(nextDir);

        while(timer < duration) {
            timer += Time.deltaTime;

            LightDirection = Vector2.Lerp(startingDir, nextDir, timer / duration);

            Debug.Log(LightDirection);
            yield return new WaitForEndOfFrame();
        }

        LightDirection = nextDir;
    }
}

[Serializable]
public struct ColorTheme
{
    public Color Primary;
    public Color Secondary;
    public Color PrimaryLight;
    public Color PrimaryDark;
    public Color Accent;
}
