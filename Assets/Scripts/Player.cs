﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour
{
    [SerializeField]
    float maxSpeed = 5;

    [SerializeField]
    float speedGrowth = 0.1f;

    float crushDist = .2f;
    float backlash = .1f;

    Vector2 input = new Vector2();
    Rigidbody2D body;

    bool isDead = false;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (isDead)
            return;

        input.x = Input.GetAxis("Horizontal");
        input.y = Input.GetAxis("Vertical");
    }

    private void FixedUpdate()
    {
        if (isDead)
            return;

        body.velocity = input * maxSpeed;

        var topCollide = Physics2D.Raycast(transform.position - Vector3.up * backlash, Vector2.up, crushDist);
        var botCollide = Physics2D.Raycast(transform.position - Vector3.down * backlash, Vector2.down, crushDist);
        var rightCollide = Physics2D.Raycast(transform.position - Vector3.right * backlash, Vector2.right, crushDist);
        var leftCollide = Physics2D.Raycast(transform.position - Vector3.left * backlash, Vector2.left, crushDist);

        if ((topCollide && botCollide) || (leftCollide && rightCollide) || (topCollide && leftCollide && rightCollide && botCollide))
            Die();
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.blue;
    //    Gizmos.DrawLine(transform.position, transform.position - (Vector3.up * backlash) + (Vector3)(Vector2.up * crushDist));
    //    Gizmos.DrawLine(transform.position, transform.position - (Vector3.down * backlash) + (Vector3)(Vector2.down * crushDist));
    //    Gizmos.DrawLine(transform.position, transform.position - (Vector3.right * backlash) + (Vector3)(Vector2.right * crushDist));
    //    Gizmos.DrawLine(transform.position, transform.position - (Vector3.left * backlash) + (Vector3)(Vector2.left * crushDist));
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var pickup = collision.GetComponent<Pickup>();

        if (pickup == null)
            return;

        switch (pickup.tag)
        {
            case "Coin":
                GameController.Instance.Score++;
                break;
            case "Speed":
                maxSpeed += speedGrowth;
                break;
        }

        pickup.Picked();
    }

    void Die()
    {
        GetComponent<Shadow>().Disable();
        GetComponent<Collider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<AudioSource>().Play();
        GetComponentInChildren<ParticleSystem>().Play();
        isDead = true;
        body.velocity = Vector2.zero;
        GameController.Instance.OnPlayerDeath();
    }
}
