﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public event OnIntegerChanged OnScoreChanged;
    public event OnIntegerChanged OnRoomCountChanged;
    public event OnIntegerChanged OnLevelChanged;

    static GameController instance;
    static ThemeManager themeManagerInstance;
    public static GameController Instance { get { return instance; } }
    public static ThemeManager ThemeManager { get { return themeManagerInstance; } }

    const float INITIAL_SPEED = 0.1f;
    //const float GROWTH_PER_LEVEL = 0.01f;

    float speedGrowth = 0.01f;

    float wallClosureSpeed = INITIAL_SPEED;

    int totalRooms = 0;
    int roomCount = 0;
    int level = 0;

    [SerializeField]
    int roomsForLevelChange = 10;

    [SerializeField]
    int levelMedium = 5;

    [SerializeField]
    int levelAdvanced = 10;

    [SerializeField]
    GameObject roomPrefab;

    [SerializeField]
    GameObject[] roomObjectPrefabs = new GameObject[1];

    [SerializeField]
    GameObject[] roomObjectPatterns = new GameObject[4];

    [SerializeField]
    RuntimeAnimatorController[] animatorsEasy = new RuntimeAnimatorController[4];

    [SerializeField]
    RuntimeAnimatorController[] animatorsMedium = new RuntimeAnimatorController[3];

    [SerializeField]
    RuntimeAnimatorController[] animatorsAdvanced = new RuntimeAnimatorController[2];

    Room currentRoom;

    List<GameObject> adjacentRooms = new List<GameObject>();
    List<RuntimeAnimatorController> currentAnimators = new List<RuntimeAnimatorController>();

    int adjacentOffset = 10;

    int score = 0;

    public int Score { get { return score; } set { score = value; if (OnScoreChanged != null) OnScoreChanged(score); } }

    AudioSource[] backgroundMusicTracks;

    private void Awake()
    {
        instance = this;
        themeManagerInstance = GetComponent<ThemeManager>();
    }

    private void Start()
    {
        currentAnimators.Clear();
        currentAnimators.AddRange(animatorsEasy);
        animatorsEasy = null;

        backgroundMusicTracks = GetComponents<AudioSource>();
        OnLevelChanged += (level) =>
        {
            foreach (var track in backgroundMusicTracks)
                track.pitch += speedGrowth;

            if (level >= levelMedium && backgroundMusicTracks[1].volume < 1)
                backgroundMusicTracks[1].volume += 0.2f;
        };
    }

    private void OnDestroy()
    {
        instance = null;
    }

    public void OnPlayerDeath()
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        KongregateAPI.SubmitStat("Score", score);
        KongregateAPI.SubmitStat("Rooms", totalRooms);
#endif

        backgroundMusicTracks[0].Stop();
        backgroundMusicTracks[1].Stop();
        backgroundMusicTracks[2].Play();

        StartCoroutine(WaitThenReload());
    }

    public void EnterRoom(Room room)
    {
        if (currentRoom == room)
            return;

        CountRoom(room);
        
        if (currentRoom != null)
        {
            currentRoom.OnPlayerLeave();
            Score += 5;

            for (int i = 0; i < adjacentRooms.Count; i++)
            {
                if (room.gameObject != adjacentRooms[i])
                    adjacentRooms[i].GetComponent<Room>().Hide();
            }

            adjacentRooms.Clear();
            adjacentRooms.Add(currentRoom.gameObject);
        }

        currentRoom = room;
    }

    void CountRoom(Room room)
    {
        roomCount++;
        totalRooms++;

        if (OnRoomCountChanged != null)
            OnRoomCountChanged(totalRooms);

        if (roomCount < roomsForLevelChange)
            return;

        roomCount = 0;
        level++;
        wallClosureSpeed += speedGrowth;

        //Each level, spawn a speedup
        var obj = InstantiateObject("Speed");
        if (obj != null)
            obj.transform.SetParent(room.transform, false);

        if (OnLevelChanged != null)
            OnLevelChanged(level);

        if (level >= levelMedium && animatorsMedium != null)
        {
            currentAnimators.AddRange(animatorsMedium);
            animatorsMedium = null;
        }

        if (level >= levelAdvanced && animatorsAdvanced != null)
        {
            currentAnimators.AddRange(animatorsAdvanced);
            animatorsAdvanced = null;
        }
    }

    public GameObject InstantiateObject(string tag)
    {
        foreach (var prefab in roomObjectPrefabs)
            if (prefab.CompareTag(tag))
                return Instantiate(prefab);

        return null;
    }

    GameObject InstantiateRandomRoom()
    {
        var obj = Instantiate(roomPrefab);

        var type = Random.Range(0, currentAnimators.Count);
        var pattern = Random.Range(0, roomObjectPatterns.Length);

        obj.GetComponent<Room>().Set(currentAnimators[type], wallClosureSpeed, roomObjectPatterns[pattern]);

        return obj;
    }

    public void CreateRoom(Vector3 doorPosition)
    {
        var enterDirection = doorPosition - currentRoom.transform.position;

        var obj = InstantiateRandomRoom();
        obj.transform.SetParent(transform);
        obj.transform.position = currentRoom.transform.position + (enterDirection.normalized * adjacentOffset);

        // Rotates the room so the left door is always attached to the current room
        if (enterDirection.x > -2 && enterDirection.x < 2)
        {
            if (enterDirection.y > 0)
                obj.transform.Rotate(new Vector3(0, 0, 90));
            else
                obj.transform.Rotate(new Vector3(0, 0, -90));
        }
        else if (enterDirection.x < 0)
            obj.transform.Rotate(new Vector3(0, 0, 180));

        adjacentRooms.Add(obj);
    }

    IEnumerator WaitThenReload()
    {
        yield return new WaitForSeconds(4);

        SceneManager.LoadScene(0);
    }
}
