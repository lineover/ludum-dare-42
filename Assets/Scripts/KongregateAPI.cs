
using UnityEngine;
using System.Runtime.InteropServices;

public class KongregateAPI : MonoBehaviour
{
#if UNITY_WEBGL && !UNITY_EDITOR
    private static KongregateAPI instance;

    [DllImport("__Internal")]
    private static extern void KAPIInit();

    [DllImport("__Internal")]
    public static extern void SubmitStat(string StatName, int StatValue);

    public void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        gameObject.name = "KongregateAPI";

        KAPIInit();
    }
#else
    private void Start()
    {
        Destroy(gameObject);
    }
#endif
}