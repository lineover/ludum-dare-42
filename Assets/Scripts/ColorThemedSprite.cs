﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class ColorThemedSprite : MonoBehaviour
{
    [SerializeField]
    ThemeColorName usedColor = ThemeColorName.Primary;

    private void Start()
    {
        Color color = Color.white;
        switch (usedColor)
        {
            case ThemeColorName.Primary:
                color = GameController.ThemeManager.CurrentTheme.Primary;
                break;
            case ThemeColorName.Secondary:
                color = GameController.ThemeManager.CurrentTheme.Secondary;
                break;
            case ThemeColorName.PrimaryLight:
                color = GameController.ThemeManager.CurrentTheme.PrimaryLight;
                break;
            case ThemeColorName.PrimaryDark:
                color = GameController.ThemeManager.CurrentTheme.PrimaryDark;
                break;
            case ThemeColorName.Accent:
                color = GameController.ThemeManager.CurrentTheme.Accent;
                break;
        }
        GetComponent<SpriteRenderer>().color = color;
    }
}
