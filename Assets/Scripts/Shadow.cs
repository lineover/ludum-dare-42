﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Shadow : MonoBehaviour
{
    Transform shadow;

    //[SerializeField]
    Color color = new Color(0, 0, 0, 0.4f);

    [SerializeField]
    float height = .03f;

    Vector3 offset;

    void Start()
    {
        var sprite = GetComponent<SpriteRenderer>().sprite;
        
        shadow = new GameObject("shadow").transform;
        shadow.transform.SetParent(transform, false);

        var renderer = (SpriteRenderer)shadow.gameObject.AddComponent(typeof(SpriteRenderer));
        renderer.sprite = sprite;
        renderer.color = color;
        renderer.sortingOrder = 2;

        GameController.ThemeManager.OnLightDirectionChanged += OnLightChanged;

        OnLightChanged(GameController.ThemeManager.LightDirection);
    }

    void OnLightChanged(Vector2 lightDirection)
    {
        shadow.transform.localPosition = lightDirection * height;
        offset = shadow.transform.localPosition;
    }

    private void Update()
    {
        shadow.transform.position = transform.position + offset;
    }

    public void Disable()
    {
        if (shadow == null)
            return;

        shadow.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        Destroy(shadow.gameObject);
        GameController.ThemeManager.OnLightDirectionChanged -= OnLightChanged;
    }
}
