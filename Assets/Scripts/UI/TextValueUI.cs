﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextValueUI : TextLanguage
{
    float value;
    public float Value
    {
        get { return value; }
        set
        {
            this.value = value;
            textComponent.text = string.Format(format, this.value);
        }
    }

    string format;

    protected override void Start()
    {
        base.Start();

        format = textComponent.text;
        Value = value;
    }
}
