﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextLanguage : MonoBehaviour {

    protected Text textComponent;
    
    protected virtual void Start () {
        textComponent = GetComponent<Text>();
	}
}
