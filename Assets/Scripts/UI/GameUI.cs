﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour {

    [SerializeField]
    TextValueUI scoreText;
    [SerializeField]
    TextValueUI roomsText;
    [SerializeField]
    TextValueUI levelText;
    void Start () {
        GameController.Instance.OnScoreChanged += (score) => scoreText.Value = score;
        GameController.Instance.OnRoomCountChanged += (count) => roomsText.Value = count;
        GameController.Instance.OnLevelChanged += (level) => levelText.Value = level;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
