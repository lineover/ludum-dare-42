﻿using System;
using UnityEngine;

public class Room : MonoBehaviour
{
    Animator anim;

    bool used;

    [SerializeField]
    Door leftDoor;

    AudioSource[] audios;

    private void Start()
    {
        audios = GetComponents<AudioSource>();

        if (anim != null)
            return;

        anim = GetComponent<Animator>();
        anim.SetFloat("Speed", 0.1f);
    }

    public void Set(RuntimeAnimatorController animator, float speed, GameObject patternPrefab)
    {
        anim = GetComponent<Animator>();
        anim.runtimeAnimatorController = animator;
        anim.SetFloat("Speed", speed);

        GetComponentInChildren<RoomObjects>().Set(patternPrefab);

        leftDoor.Disable();
    }

    public void OnHidden()
    {
        if (used)
            Destroy(gameObject);
    }

    public void StopWalls()
    {
        anim.SetFloat("Speed", 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (used)
            return;

        anim.Play("Close");
        anim.SetBool("Visible", true);
        GameController.Instance.EnterRoom(this);
    }

    public void OnPlayerLeave()
    {
        //TODO excluir as moedas?
        used = true;
    }

    public void PlayHit()
    {
        audios[1].Play();
    }

    public void PlayEngineSound()
    {
        audios[0].Play();
    }

    public void StopEngineSound()
    {
        audios[0].Stop();
    }

    public void Hide()
    {
        anim.SetBool("Visible", false);
    }
}