﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource)), RequireComponent(typeof(ParticleSystem)), RequireComponent(typeof(SpriteRenderer)), RequireComponent(typeof(Collider2D)), RequireComponent(typeof(Shadow))]
public class Pickup : MonoBehaviour
{
    public void Picked()
    {
        GetComponent<Shadow>().Disable();
        GetComponent<Collider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<AudioSource>().Play();
        var pSystem = GetComponent<ParticleSystem>();
        pSystem.Play();
        StartCoroutine(WaitThenDestroy(pSystem));
    }

    IEnumerator WaitThenDestroy(ParticleSystem pSystem)
    {
        yield return new WaitWhile(() => pSystem.IsAlive());

        Destroy(gameObject);
    }
}
