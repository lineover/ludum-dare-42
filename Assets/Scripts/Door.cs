﻿using System;
using UnityEngine;

public class Door : MonoBehaviour
{
    bool isEnteringRoom = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isEnteringRoom)
            return;

        GetComponent<Animator>().Play("Close");
        GameController.Instance.CreateRoom(transform.position);
    }

    public void Disable()
    {
        isEnteringRoom = true;
    }
}
